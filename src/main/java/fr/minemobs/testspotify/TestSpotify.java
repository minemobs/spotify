package fr.minemobs.testspotify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSpotify {

    public static void main(String[] args) {
        SpringApplication.run(TestSpotify.class, args);
    }
}
