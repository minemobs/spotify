package fr.minemobs.testspotify;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.SpotifyHttpManager;
import org.springframework.stereotype.Service;

import java.net.URI;

@Service
public class SpotifyAPIClient {

    public static final String clientId = "9c8df43a67714822a437fe13f2a609c2";
    public static final String clientSecret = "78208caa25f74bbc8a1569c1f259ea1f";
    private static final URI redirectUri = SpotifyHttpManager.makeUri("http://localhost:8080/callback");
    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setClientId(clientId)
            .setClientSecret(clientSecret)
            .setRedirectUri(redirectUri)
            .build();

    public SpotifyApi api(){
        return spotifyApi;
    }
}
