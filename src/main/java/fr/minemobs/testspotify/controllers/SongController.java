package fr.minemobs.testspotify.controllers;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.miscellaneous.CurrentlyPlaying;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.data.player.GetUsersCurrentlyPlayingTrackRequest;
import fr.minemobs.testspotify.SpotifyAPIClient;
import org.apache.hc.core5.http.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
public class SongController {

    @Autowired
    private SpotifyAPIClient apiClient;

    @GetMapping(value="/currentSong")
    @ResponseBody
    public String currentSong() {
        SpotifyApi spotifyApi = apiClient.api();
        try {
            Track track = (Track) spotifyApi.getUsersCurrentlyPlayingTrack().build().execute().getItem();
            System.out.println(track.getName());
        } catch (IOException | SpotifyWebApiException | ParseException exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
