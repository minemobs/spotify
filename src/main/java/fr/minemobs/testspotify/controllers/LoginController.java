package fr.minemobs.testspotify.controllers;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeUriRequest;
import fr.minemobs.testspotify.SpotifyAPIClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;

import java.net.URI;

@Controller
public class LoginController {

    @Autowired
    private SpotifyAPIClient apiClient;

    @GetMapping(value = "/login")
    public RedirectView login(){
        SpotifyApi spotifyApi = apiClient.api();
        AuthorizationCodeUriRequest authorizationCodeUriRequest = spotifyApi.authorizationCodeUri()
                .scope("playlist-modify-public,playlist-modify-private,playlist-read-private,playlist-read-collaborative,user-library-modify,user-library-read," +
                        "user-read-currently-playing,user-read-playback-state,user-read-playback-position,user-read-recently-played,user-top-read").build();

        final URI uri = authorizationCodeUriRequest.execute();

        System.out.println("URI: " + uri.toString());
        return new RedirectView(uri.toString());
    }

}
